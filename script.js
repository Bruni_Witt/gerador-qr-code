const inputValue = document.querySelector('#inputValue'); /*Id do formulário*/
const btnValue = document.querySelector('#btnValue'); /*Id do botão de gerar QR Code*/
const imgQrCode = document.querySelector('#imgQrCode'); /*Id da Img do QR Code */
const wrapper = document.querySelector('.wrapper'); /*Class do body*/
const esconder = document.getElementById('esconder'); /* Id do "botão" de Início */
const baixar = document.getElementById('baixar');
let valueDefault;

function toggle(){
    if (esconder.style.display === 'none'){
        esconder.style.display = 'block';
    }else{
        esconder.style.display = 'block';
    }};

function botao_download(){
    if(baixar.style.display === 'none'){
        baixar.style.disple = 'block';
    }else{
        baixar.style.display = 'block';
    }};

function checkUrl(string) {
    try {
        const url = new URL(string)
        console.log("Valid URL!")
        return true;
    } catch(err) {
        console.log("Invalid URL!")
        return false;
    }}

function abrirMenu(){
    document.getElementById('btnMenu').style.width = '250px';
};

function fecharMenu(){
    document.getElementById('btnMenu').style.width = '0px';
}




btnValue.addEventListener('click', () => {           /*adiciona um evento de click no botão de gerar QR CODE */
    const qrcodeValue = inputValue.value.trim();   /*obter o valor do campo do formulário tirando espaços em branco*/
    if(!qrcodeValue || qrcodeValue === valueDefault) return; //o !qrcodevalue seria o NADA, então caso o qrcodeValue for nada também, ele retorna o valueDefault
    valueDefault = qrcodeValue;  
    if (checkUrl(inputValue.value)) {
        btnValue.innerText = 'Gerando QR Code...';               /*O texto do botão vai mudar */
        imgQrCode.src = `https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${valueDefault}`; /*vai buscar na API e criar uma com o valueDefault */
        imgQrCode.addEventListener('load', () => { /*colocado o evento load, mostrando que a pagina carregou por completo */
        wrapper.classList.add('active');       /*adiciona a classe active no elemento wrapper*/
        btnValue.innerText = 'Gerar QRCode';   /*escreve um texto no botão do QR Code btnValue*/
        toggle();                                /* puxa a function toggle, que adiciona um botao início na tela pós load (id esconder)*/
        botao_download();
    });
    } else {
        alert("Digite uma URL válida");
        valueDefault
    }
    
});

async function downloadImage() {
    const imageSrc = `https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=${valueDefault}`
    const image = await fetch(imageSrc);
    const imageBlob = await image.blob();
    const imageURL = URL.createObjectURL(imageBlob);

    const link = document.createElement('a');
    link.href = imageURL;;
    link.download = 'imagem.jpg';
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  const downloadButton = document.getElementById('baixar');
  downloadButton.addEventListener('click', () => {
    downloadImage();
  });


